let notifications = "2";

let campaigns = [
  {
    id: "1",
    image:
      "https://cdn.sanity.io/images/c1chvb1i/production/25b2a524445a342f595aa7cc01896db655746598-640x426.jpg?rect=36,0,568,426",
    name: "NIKE Sneaker Campaign Summer II",
    influencers: "9",
    closes: "3",
    status: "active",
    reach: "12576",
    likes: "1789",
    comments: "876",
    opens: "10782",
    screens: "789",
    completion: "67"
  },
  {
    id: "2",
    image:
      "https://i.pinimg.com/236x/7e/ba/e2/7ebae25393d13eb5dd8928f036228910.jpg",
    name: "#DOYOGA Campaign",
    influencers: "11",
    closes: "8",
    status: "active",
    reach: "34567",
    likes: "5789",
    comments: "234",
    opens: "",
    screens: "",
    completion: ""
  }
];

let statistics = {
posts: "123",
likes: "5789",
snaps: "34567",
opens: "5789"
};

let values = [
  { name: "MON", pv: 5 },
  { name: "TUE", pv: 25 },
  { name: "WED", pv: 15 },
  { name: "THU", pv: 65 },
  { name: "FRI", pv: 25 },
  { name: "SAT", pv: 100 },
  { name: "SUN", pv: 50 }
];

let todos = [
  {
    id: "1",
    image: "https://1.bp.blogspot.com/-vBBphadGElw/VMgwsBJfoWI/AAAAAAAAAC8/Ra7YSjB90Ng/s1600/Manrepeller-selfie-instagram-paula-joye-lifestyled1.jpg",
    name: "Ellaria Dorne",
    campaign_name: "'NIKE Sneaker Campaign Summer II'",
    action: "uploaded instagram contet for proof to your campign ",
    date: "02.07.1016",
    content: "Who sasy you can't be elegant in sneakers?! :)",
    hashtags:
      "#nike #sneakers #morningoutfit #liveyourlife #fashion #dailyfashion #fashionista"
  },
  {
    id: "2",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTfOSehaDOaQNg2lM-LD_uYF0_t7r1cNa5ItiH1UWQH7ZhuYrkr&usqp=CAU",
    name: "Amy Champeon",
    campaign_name: "'Tennis clothing for men'",
    action: "just applied to your open campaign",
    date: "",
    content: "Who sasy you can't be elegant in sneakers?! :)",
    hashtags:
      "#nike #sneakers #morningoutfit #liveyourlife #fashion #dailyfashion #fashionista"
  },
  {
    id: "3",
    image: "https://m.media-amazon.com/images/S/abs-image-upload-na/d/AmazonStores/ATVPDKIKX0DER/493fb72d88e2934ffbf28498937c1f04.w480.h318._CR70%2C0%2C318%2C318_SX375_SY375_.jpg",
    name: "Mike Jones",
    campaign_name: "'#DOUYOUYOGA'",
    action: "published content to instagram for campign",
    date: "02.07.1016",
    content: "My new yoga pants from Nike has arrived, I adore it!",
    hashtags:
      "#nike #yoga #sun  #namaste #liveyourlife #fashion #dailyfashion #sport #sporty #nature"
  }
];

let top_influencers = [{
    id: "1",
    image: "https://p2.trrsf.com/image/fget/cf/600/400/images.terra.com/2014/08/20/mujer-sonrisa-feliz.jpg",
    name: "Mildred Estrada",
    percentage: "25"
  }, {
    id: "2",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQAm8EJPrsjrYENDmqAF6WGxZsh9LZHuwg6Q2ja2WznbgeY2xMKQ&s",
    name: "Charlotte Morris",
    percentage: "20"
  }, {
    id: "3",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw4QLr6jLeDGGll14-YdS-lsQhlodqtDYdy0oIutpWtAgNviBB6A&s",
    name: "Sadie Brooks",
    percentage: "18"
  }, {
    id: "4",
    image: "https://a.wattpad.com/useravatar/wadduphomie.128.919816.jpg",
    name: "Mary Nichols",
    percentage: "18"
  }, {
    id: "5",
    image: "https://i.pinimg.com/280x280_RS/33/93/d7/3393d7952becfe6b549d4765ab5ec2fd.jpg",
    name: "Teresa Austin",
    percentage: "17"
  }
];

export function fetchStatistics() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(statistics)
    }, 300)
  })
} 
export function fetchValues() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(values)
    }, 300)
  })
} 
export function fetchNotifications() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(notifications)
    }, 300)
  })
} 

export function fetchCampaigns() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(campaigns)
    }, 300)
  })
}
export function fetchTodos() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(todos)
    }, 300)
  })
}
export function fetchTopInfluencers() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(top_influencers)
    }, 300)
  })
}

